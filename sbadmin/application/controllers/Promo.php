<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promo extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('model_global');

        $session = $_SESSION['user'];
        if (!$session) {
            redirect(base_url('login'));
        }
    }
    
    public function index()
	{
        $data = [];

        // get product
        $data['promo'] = $this->model_global->get_promo();

        // application/views/templates/header.php
		$this->load->view('templates/header', $data);
        
        // application/views/v_product.php
        $this->load->view('v_promo', $data);

        // application/views/templates/footer.php
		$this->load->view('templates/footer', $data);
    }
    
    public function add()
	{
        $data = [];

        // application/views/templates/header.php
		$this->load->view('templates/header', $data);
        
        // application/views/v_product_add.php
        $this->load->view('v_promo_add', $data);

        // application/views/templates/footer.php
		$this->load->view('templates/footer', $data);
    }
    
    public function edit()
	{
        $req = $_REQUEST;
        $data = [];

        // get single product
        $data['promo'] = $this->model_global->get_promo_by_id($req['id']);

        // application/views/templates/header.php
        $this->load->view('templates/header', $data);
        
        // application/views/v_product_edit.php
        $this->load->view('v_promo_edit', $data);

        // application/views/templates/footer.php
        $this->load->view('templates/footer', $data);
    }

    public function delete()
	{
        $req = $_REQUEST;
        $data = [];

        // delete product
        $data = $this->model_global->delete_promo($req['id']);
        if ($data) {
            echo "
            <script>
                alert('Promo has been deleted!');
                location.href = '".base_url()."promo';
            </script>
            ";
        
        } else {
            echo "
            <script>
                alert('Promo has not been deleted, please try again later.');
                location.href = '".base_url()."promo';
            </script>
            ";
        }
    }

    public function action_add()
    {
        $params = $this->input->post();
        $files = $_FILES;
 
        $file_name = $_FILES['image_promo']['name']; // NAMA FILE + FORMAT
        $temporary_dir_file = $_FILES['image_promo']['tmp_name']; // TEMPORARY DIREKTORI
 
        $lokasi_upload_file = "./assets/image_promo/"; // DIREKTORI UNTUK NYIMPAN FILE
 
        $uploaded = move_uploaded_file($temporary_dir_file, $lokasi_upload_file.$file_name); // MEKANIMSE UPLOAD FILE
 
        // VALIDASI KALAU UPLOAD BERHASIL / GAGAL
        if ($uploaded) {
            $params['image_promo'] = base_url().'assets/image_promo/'.$file_name;
        }
        $data = $this->model_global->create_promo($params);
        if ($data) {
            echo "
            <script>
                alert('Promo has been saved!');
                location.href = '".base_url()."promo';
            </script>
            ";
        
        } else {
            echo "
            <script>
                alert('Promo has not been saved, please try again later.');
                location.href = '".base_url()."promo';
            </script>
            ";
        }
    }

    public function action_edit()
    {
        $params = $this->input->post();
        $files = $_FILES;
 
        $file_name = $_FILES['image_promo']['name']; // NAMA FILE + FORMAT
        $temporary_dir_file = $_FILES['image_promo']['tmp_name']; // TEMPORARY DIREKTORI
 
        $lokasi_upload_file = "./assets/image_promo/"; // DIREKTORI UNTUK NYIMPAN FILE
 
        $uploaded = move_uploaded_file($temporary_dir_file, $lokasi_upload_file.$file_name); // MEKANIMSE UPLOAD FILE
 
        // VALIDASI KALAU UPLOAD BERHASIL / GAGAL
        if ($uploaded) {
            $params['image_promo'] = base_url().'assets/image_promo/'.$file_name;
        }
        $data = $this->model_global->update_promo($params['id'], $params);
        if ($data) {
            echo "
            <script>
                alert('Promo has been saved!');
                location.href = '".base_url()."promo';
            </script>
            ";
        
        } else {
            echo "
            <script>
                alert('Promo has not been saved, please try again later.');
                location.href = '".base_url()."promo';
            </script>
            ";
        }
    }
}