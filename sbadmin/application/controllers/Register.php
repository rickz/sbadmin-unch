<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('model_global');
    }
    
    public function index()
    {
        $data = [];

        $this->load->view('v_register');
    }
    
    public function validate()
    {
        $params = $this->input->post();

        // validate login
        $data = $this->model_global->validate_login($params);
        if ($data) {
            $new_session = ['user' => $data];
            $this->session->set_userdata($new_session);

            echo "
            <script>
                location.href = '".base_url()."dashboard';
            </script>
            ";
        
        } else {
            echo "
            <script>
                alert('Email / Password is incorrect, please try again.');
                location.href = '".base_url()."login';
            </script>
            ";
        }
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect(base_url('login'));
    }
}