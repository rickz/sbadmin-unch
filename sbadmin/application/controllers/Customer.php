<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('model_global');

        $session = $_SESSION['user'];
        if (!$session) {
            redirect(base_url('login'));
        }
    }
    
    public function index()
	{
        $data = [];

        // get product
        $data['customer'] = $this->model_global->get_customer();

        // application/views/templates/header.php
		$this->load->view('templates/header', $data);
        
        // application/views/v_product.php
        $this->load->view('v_customer', $data);

        // application/views/templates/footer.php
		$this->load->view('templates/footer', $data);
    }
    
    public function add()
	{
        $data = [];

        // application/views/templates/header.php
		$this->load->view('templates/header', $data);
        
        // application/views/v_product_add.php
        $this->load->view('v_customer_add', $data);

        // application/views/templates/footer.php
		$this->load->view('templates/footer', $data);
    }
    
    public function edit()
	{
        $req = $_REQUEST;
        $data = [];

        // get single product
        $data['customer'] = $this->model_global->get_customer_by_id($req['id']);

        // application/views/templates/header.php
        $this->load->view('templates/header', $data);
        
        // application/views/v_product_edit.php
        $this->load->view('v_customer_edit', $data);

        // application/views/templates/footer.php
        $this->load->view('templates/footer', $data);
    }

    public function delete()
	{
        $req = $_REQUEST;
        $data = [];

        // delete product
        $data = $this->model_global->delete_customer($req['id']);
        if ($data) {
            echo "
            <script>
                alert('Customer has been deleted!');
                location.href = '".base_url()."customer';
            </script>
            ";
        
        } else {
            echo "
            <script>
                alert('Customer has not been deleted, please try again later.');
                location.href = '".base_url()."customer';
            </script>
            ";
        }
    }

    public function action_add()
    {
        $params = $this->input->post();
        $data = $this->model_global->create_customer($params);
        if ($data) {
            echo "
            <script>
                alert('Customer has been added!');
                location.href = '".base_url()."customer';
            </script>
            ";
        
        } else {
            echo "
            <script>
                alert('Customer has not been added, please try again later.');
                location.href = '".base_url()."customer';
            </script>
            ";
        }
    }

    public function action_edit()
    {
        $params = $this->input->post();
        $data = $this->model_global->update_customer($params['id'], $params);
        if ($data) {
            echo "
            <script>
                alert('Customer has been edited!');
                location.href = '".base_url()."customer';
            </script>
            ";
        
        } else {
            echo "
            <script>
                alert('Customer has not been edited, please try again later.');
                location.href = '".base_url()."customer';
            </script>
            ";
        }
    }
}