<?php
class model_global extends CI_Model {
    public function __construct()
    {
        $this->load->database();
    }

    public function get_promo()
    {
        $result = $this->db->get('promo')->result_array();
		
		return $result;
	}

	public function get_customer()
    {
        $result = $this->db->get('customer')->result_array();
		
		return $result;
	}

	public function get_promo_by_id($id)
    {
		$this->db->where('id', $id);
        $result = $this->db->get('promo')->row_array();
		
		return $result;
	}

	public function get_customer_by_id($id)
    {
		$this->db->where('id', $id);
        $result = $this->db->get('customer')->row_array();
		
		return $result;
	}

	public function create_promo($data)
    {
		$result = $this->db->insert('promo', $data);
		
		return $result;
	}

	public function create_customer($data)
    {
		$result = $this->db->insert('customer', $data);
		
		return $result;
	}

	public function update_promo($id, $data)
    {
		$this->db->where('id', $id);
		$result = $this->db->update('promo', $data);
		
		return $result;
	}

	public function update_customer($id, $data)
    {
		$this->db->where('id', $id);
		$result = $this->db->update('customer', $data);
		
		return $result;
	}

	public function delete_promo($id)
	{
		$this->db->where('id', $id);
		$result = $this->db->delete('promo');
		
		return $result;
	}

	public function delete_customer($id)
	{
		$this->db->where('id', $id);
		$result = $this->db->delete('customer');
		
		return $result;
	}

	public function validate_login($data)
	{
		$salt = '*/123_pass*/';
		$salt_2 = '#334)(00123';
		$password = md5($salt.$data['password'].$salt_2);

		$this->db->where('email', $data['email']);
		$this->db->where('password', $password);
		$result = $this->db->get('users')->row_array();

		return $result;
	}

	public function update_profile($data)
	{
		// Ini kalau misalnya password enggak ada perubahan
		// Maka jangan ikut sertakan password tersebut
		if ($data['password'] == '         ') {
			unset($data['password']);
		
		// Kalau ada perubahan maka ikut sertakan
		// Dan update Encrypt md5 passwordnya
		} else {
			$salt = '*/123_pass*/';
			$salt_2 = '#334)(00123';
			$password = md5($salt.$data['password'].$salt_2);
			$data['password'] = $password;
		}

		// Mekanisme update ke database
		$this->db->where('id', $data['id']);
		$result = $this->db->update('users', $data);

		// Kemudian untuk mengupdate sebuah session
		// Yang disimpan di RAM
		// Kita ambil ulang data nya dari database seperti
		// Pengambilan Single Data
		$this->db->where('id', $data['id']);
		$user_data = $this->db->get('users')->row_array();

		// Kalau sudah kita set ulang seperti di login tadi
		$new_session = ['user' => $user_data];
		$this->session->set_userdata($new_session);
		
		return $result;
	}
}