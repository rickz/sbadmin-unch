<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_crud extends CI_Model{
    function __construct()
    {
        $this->load->database();
    }

    function get_data()
    {
        $data = $this->db->get('cscart_hartono_mobile_banner')->result_array();
        return $data;
    }

    function get_data_by_id($id)
    {
        $this->db->where('id', $id);
        $data = $this->db->get('cscart_hartono_mobile_banner')->row_array();
        return $data;
    }
    
    function insert_data($data)
    {
        $result = $this->db->insert('cscart_hartono_mobile_banner', $data);
        return $result;
    }

    function update_data($id, $data)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('cscart_hartono_mobile_banner', $data);
        return $result;
    }

    function delete_data($id)
    {
        $this->db->where('id', $id);
        $result = $this->db->delete('cscart_hartono_mobile_banner');
        return $result;
    }
}