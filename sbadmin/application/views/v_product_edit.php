
        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Product</h1>
            <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
          </div>

          <!-- Content Row -->

          <div class="row">

            <!-- Area Chart -->
            <div class="col-xl-12 col-lg-12">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Edit Product</h6>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Action:</div>
                      <a class="dropdown-item" href="<?php echo base_url(); ?>product/add">Add Product</a>
                    </div>
                  </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                <form method="POST" action="<?php echo base_url(); ?>product/action_edit">
                <input type="hidden" name="id" value="<?php echo $product['id']; ?>" />
            
                <div class="form-group">
                <label>SKU <font color="red">*</font></label>
                <input required value="<?php echo $product['sku']; ?>" type="text" name="sku" class="form-control" />
            </div>
            <div class="form-group">
                <label>Product Name <font color="red">*</font></label>
                <input required value="<?php echo $product['product_name']; ?>" type="text" name="product_name" class="form-control" />
            </div>
            <div class="form-group">
                <label>Buy Price <font color="red">*</font></label>
                <input required value="<?php echo $product['buy_price']; ?>" type="number" name="buy_price" class="form-control" />
            </div>
            <div class="form-group">
                <label>Sell Price <font color="red">*</font></label>
                <input required value="<?php echo $product['sell_price']; ?>" type="number" name="sell_price" class="form-control" />
            </div>
            <div class="form-group">
                <label>Product Image URL <font color="red">*</font></label>
                <input required value="<?php echo $product['image_product']; ?>" type="text" name="image_product" class="form-control" />
            </div>
            <div class="form-group">
                <input type="submit" value="Save" class="btn btn-md btn-primary" />
            </div>
        </form>

                </div>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

     