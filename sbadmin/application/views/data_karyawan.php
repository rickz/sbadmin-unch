<html>
    <h1><?php echo $judul; ?></h1>
    <a href="<?php echo base_url(); ?>data_karyawan/tambah">Tambah Data</a><br /><br />
    <table border="1">
        <tr>
            <td>Nama Lengkap</td>
            <td>Nama Panggilan</td>
            <td>Email</td>
            <td>Umur</td>
            <td>Jabatan</td>
            <td>Aksi</td>
        </tr>
        <?php foreach ($data_karyawan as $key => $d) { ?>
            <tr>
                <td><?php echo $d['nama_lengkap']; ?></td>
                <td><?php echo $d['nama_panggilan']; ?></td>
                <td><?php echo $d['email']; ?></td>
                <td><?php echo $d['umur']; ?></td>
                <td><?php echo $d['jabatan']; ?></td>
                <td>
                    <a href="<?php echo base_url(); ?>data_karyawan/edit?id=<?php echo $d['id']; ?>">
                        Edit
                    </a>
                    |
                    <a href="<?php echo base_url(); ?>data_karyawan/delete?id=<?php echo $d['id']; ?>">
                        Delete
                    </a>
                </td>
            </tr>
        <?php } ?>
    </table>
</html>