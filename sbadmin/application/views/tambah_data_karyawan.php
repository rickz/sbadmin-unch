<html>
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script> -->

    <h1>Data Karyawan</h1>
    <a href="<?php echo base_url(); ?>data_karyawan">Data Karyawan</a><br /><br />
    <div class="container">
        <form method="POST" action="<?php echo base_url(); ?>data_karyawan/action_tambah">
            <div class="form-group">
                <label>Nama Lengkap</label>
                <input type="text" name="v_nama_lengkap" class="form-control" />
            </div>
            <div class="form-group">
                <label>Nama Panggilan</label>
                <input type="text" name="v_nama_panggilan" class="form-control" />
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="email" name="v_email" class="form-control" />
            </div>
            <div class="form-group">
                <label>Alamat</label>
                <textarea name="v_alamat" class="form-control"></textarea>
            </div>
            <div class="form-group">
                <label>Umur</label>
                <input type="text" name="v_umur" class="form-control" />
            </div>
            <div class="form-group">
                <label>Jabatan</label>
                <input type="text" name="v_jabatan" class="form-control" />
            </div>
            <div class="form-group">
                <label>Gaji</label>
                <input type="text" name="v_gaji" class="form-control" />
            </div>
            <div class="form-group">
                <input type="submit" name="simpan" value="Simpan" />
            </div>
        </form>
    </div>
</html>