<!-- Footer -->
<footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Coretan Otaku 2020</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="profileModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Profile</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
            <form method="POST" action="<?php echo base_url(); ?>dashboard/save_profile">
                <input value="<?php echo $_SESSION['user']['id']; ?>" type="hidden" name="id" class="form-control" />    

                <div class="form-group">
                    <label>Email <font color="red">*</font></label>
                    <input required value="<?php echo $_SESSION['user']['email']; ?>" type="email" name="email" class="form-control" />
                </div>
                <div class="form-group">
                    <label>Password <font color="red">*</font></label>
                    <input required value="         " type="password" name="password" class="form-control" />
                </div>
                <div class="form-group">
                    <label>Nama Lengkap <font color="red">*</font></label>
                    <input required value="<?php echo $_SESSION['user']['nama_lengkap']; ?>" type="nama_lengkap" name="nama_lengkap" class="form-control" />
                </div>
                <div class="form-group">
                    <input type="submit" value="Save" class="btn btn-md btn-primary" />
                </div>
            </form>
        </div>
        <!-- <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div> -->
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?php echo base_url();?>assets/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?php echo base_url();?>assets/js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="<?php echo base_url();?>assets/vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="<?php echo base_url();?>assets/js/demo/chart-area-demo.js"></script>
  <script src="<?php echo base_url();?>assets/js/demo/chart-pie-demo.js"></script>

</body>

</html>
