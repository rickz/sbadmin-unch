-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 15 Sep 2020 pada 02.02
-- Versi Server: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sbadmin`
--
CREATE DATABASE IF NOT EXISTS `sbadmin` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `sbadmin`;

-- --------------------------------------------------------

--
-- Struktur dari tabel `brand`
--

CREATE TABLE `brand` (
  `id` int(11) NOT NULL,
  `brand_name` varchar(255) NOT NULL,
  `brand_image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `brand`
--

INSERT INTO `brand` (`id`, `brand_name`, `brand_image`) VALUES
(1, 'Hero 57', 'http://localhost/sbadmin/assets/brand_image/Hero 57.jpg'),
(2, 'Juicenation', 'http://localhost/sbadmin/assets/brand_image/Juicenation.jpg'),
(3, 'Jualvape', 'http://localhost/sbadmin/assets/brand_image/Jualvape.png'),
(4, 'Vapor King', 'http://localhost/sbadmin/assets/brand_image/Vapor King.jpg'),
(5, 'Smoant', 'http://localhost/sbadmin/assets/brand_image/Smoant.png'),
(6, 'Wismec', 'http://localhost/sbadmin/assets/brand_image/Wismec.png'),
(7, 'JVS Distribution', 'http://localhost/sbadmin/assets/brand_image/JVS Distribution.jpeg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `category_description` text NOT NULL,
  `category_image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `category`
--

INSERT INTO `category` (`id`, `category_name`, `category_description`, `category_image`) VALUES
(1, 'Mod', 'Sebuah device utama untuk vaping', 'http://localhost/sbadmin/assets/category_image/Mod.jpg'),
(2, 'Liquid', 'Liquid adalah cairan utama untuk vaping dengan nicotine yang bervarian', 'http://localhost/sbadmin/assets/category_image/Liquid.jpg'),
(3, 'RDA', 'Sebuah device untuk vaping', 'http://localhost/sbadmin/assets/category_image/RDA.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `nama_depan` varchar(255) NOT NULL,
  `nama_belakang` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `umur` varchar(255) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `customer`
--

INSERT INTO `customer` (`id`, `nama_depan`, `nama_belakang`, `email`, `password`, `umur`, `alamat`) VALUES
(1, 'Erik', 'Sopia', 'eriksopian22@gmail.com', 'okehsiap123', '20', 'Jl dimana gan'),
(3, 'Siap', 'Lahs', 'siapganz817@yandex.com', 'wawawaw819', '28', 'Jl AWT');

-- --------------------------------------------------------

--
-- Struktur dari tabel `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `price` int(255) NOT NULL,
  `thumbnail_product` varchar(255) NOT NULL,
  `product_description` text NOT NULL,
  `stock_product` varchar(255) NOT NULL,
  `specifications_product` varchar(255) NOT NULL,
  `information_product` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `product`
--

INSERT INTO `product` (`id`, `product_name`, `price`, `thumbnail_product`, `product_description`, `stock_product`, `specifications_product`, `information_product`) VALUES
(3, 'American Breakfast Liquid by HERO57', 150, 'http://localhost/sbadmin/assets/thumbnail_product/1663512_45f439fa-e370-4ad7-abc4-8a442555c13a_923_923.jpg', 'Liquid ini dirilis dengan paduan cinta oleh HERO57 dengan PG 30 dan VG 70', '99', '100ml Nicotine 6mg', 'HERO57');

-- --------------------------------------------------------

--
-- Struktur dari tabel `promo`
--

CREATE TABLE `promo` (
  `id` int(11) NOT NULL,
  `nama_promo` varchar(255) NOT NULL,
  `deskripsi_promo` varchar(255) NOT NULL,
  `image_promo` text NOT NULL,
  `position` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `promo`
--

INSERT INTO `promo` (`id`, `nama_promo`, `deskripsi_promo`, `image_promo`, `position`) VALUES
(1, 'Promo Gojec', 'Diskon 50% dengan pengiriman melalui gojek*', 'http://localhost/sbadmin/assets/image_promo/16735907_1435409096477315_388294200_n.jpg', '1'),
(4, 'Promo Buroq', 'Promo saat memakai jasa buroq sampai langit ke tujuh', 'http://localhost/sbadmin/assets/image_promo/13718500_1053938031359619_2976742122330548484_n.jpg', '4');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nama_lengkap` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `nama_lengkap`) VALUES
(1, 'eriksopian22@gmail.com', '223e5eadcb1e71d1e3a39004cf10766d', 'Erik Sopian');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `promo`
--
ALTER TABLE `promo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `promo`
--
ALTER TABLE `promo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
