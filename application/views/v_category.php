<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Category</h1>
    <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
  </div>

  <!-- Content Row -->

  <div class="row">

    <!-- Area Chart -->
    <div class="col-xl-12 col-lg-12">
      <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
          <h6 class="m-0 font-weight-bold text-primary">Category Product</h6>
          <div class="dropdown no-arrow">
            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
              <!-- <div class="dropdown-header">Action:</div> -->
              <a class="dropdown-item" href="<?php echo base_url(); ?>category/add">Add Category</a>
            </div>
          </div>
        </div>
        <!-- Card Body -->
        <div class="card-body">
        <table class="table">
            <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col"><center>Category Name</center></th>
                <th scope="col"><center>Description</center></th>
                <th scope="col">Image</th>
                <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($category as $c) { ?>
                <tr>
                    <td><?php echo $c['id']; ?></td>
                    <td><center><?php echo $c['category_name']; ?></center></td>
                    <td><center><?php echo $c['category_description']; ?></center></td>
                    <td><img src="<?php echo $c['category_image']; ?>" height="50"</td>
                    <td>
                      <!-- button edit -->
                      <a href="<?php echo base_url(); ?>category/edit?id=<?php echo $c['id']; ?>">
                        <i class="fa fa-pen" style="color: blue;"></i>
                      </a> 
                      
                      <!-- space / jarak -->
                      &nbsp;&nbsp; 
                      
                      <!-- button hapush -->
                      <a href="<?php echo base_url(); ?>category/delete?id=<?php echo $c['id']; ?>">
                        <i class="fa fa-trash" style="color: red;"></i>
                      </a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
        </div>
      </div>
    </div>
  </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

