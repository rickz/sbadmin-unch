<html>
    <h1>Data Barang</h1>
    <a href="<?php echo base_url(); ?>data_barang">Tabel Barang</a><br /><br />
    <div class="container">
        <form method="POST" action="<?php echo base_url(); ?>data_barang/action_tambah">
           <div class="form-group">
                <label>Type</label>
                <input type="text" name="v_type" class="form-control" />
            </div>
            <div class="form-group">
                <label>Title</label>
                <input type="text" name="v_title" class="form-control" />
            </div>
             <div class="form-group">
                <label>Banner</label>
                <input type="text" name="v_banner_image_url" class="form-control" />
            </div>
            <div class="form-group">
                <label>Description</label>
                <textarea name="v_description" class="form-control"></textarea>
            </div>
            <div class="form-group">
                <label>Available From</label>
                <input type="number" name="v_available_from" class="form-control" />
            </div>
            <div class="form-group">
                <label>Available To</label>
                <input type="number" name="v_available_to" class="form-control" />
            </div>
            <div class="form-group">
                <label>QRCode</label>
                <input type="text" name="v_qrcode" class="form-control" />
            </div>
            <div class="form-group">
                <label>Product ID</label>
                <input type="text" name="v_product_id" class="form-control" />
            </div>
            <div class="form-group">
                <label>Category ID</label>
                <input type="text" name="v_category_id" class="form-control" />
            </div>
            <div class="form-group">
                <label>Position</label>
                <input type="number" name="v_position" class="form-control" />
            </div>
            <div class="form-group">
                <input type="submit" name="simpan" value="Simpan" />
            </div>
        </form>
    </div>
</html>