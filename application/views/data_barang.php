<html>
<h1><?php echo $judul; ?></h1>
<a href="<?php echo base_url(); ?>data_barang/tambah">Tambah Data</a><br /><br />
<table border='1'>
    <tr>
        <td><center>Type</center></td>
        <td><center>Title</center></td>
        <td><center>Banner</center></td>
        <td><center>Description</center></td>
        <td><center>Available From</center></td>
        <td><center>Available To</center></td>
        <td><center>QRCode</center></td>
        <td><center>Product ID</center></td>
        <td><center>Category ID</center></td>
        <td><center>Position</center></td>
        <td><center>Action</center></td>
    </tr>
    <?php 
    foreach ($data_barang as $key => $d) { ?>
        <tr>
            <td><center><?php echo $d['type']; ?></center></td>
            <td><center><?php echo $d['title']; ?></center></td>
            <td><center><img src=<?php echo $d['banner_image_url']; ?> style="width:50px;height:50px;"></center></td>
            <td><center><?php echo $d['description']; ?></center></td>
            <td><center><?php echo $d['available_from']; ?></center></td>
            <td><center><?php echo $d['available_to']; ?></center></td>
            <td><center><img src=<?php echo $d['qrcode_url']; ?> style="width:50px;height:50px;"></center></td>
            <td><center><?php echo $d['product_id']; ?></center></td>
            <td><center><?php echo $d['category_id']; ?></center></td>
            <td><center><?php echo $d['position']; ?></center></td>
            <td><a href="<?php echo base_url(); ?>data_barang/edit?id=<?php echo $d['id']; ?>">
                        Edit
                    </a>|
            <a href="<?php echo base_url(); ?>data_barang/delete?id=<?php echo $d['id']; ?>>">
                        Delete
                    </a></td>
        </tr>
    <?php } ?>
</table>
</html>