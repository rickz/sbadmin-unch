<html>
    <h1>Data Karyawan</h1>
    <a href="<?php echo base_url(); ?>data_karyawan">Data Karyawan</a><br /><br />
    <div class="container">
        <form method="POST" action="<?php echo base_url(); ?>data_karyawan/action_edit">
            <input type="hidden" name="v_id" value="<?php echo $data['id']; ?>" />

            <div class="form-group">
                <label>Nama Lengkap</label>
                <input value="<?php echo $data['nama_lengkap']; ?>" type="text" name="v_nama_lengkap" class="form-control" />
            </div>
            <div class="form-group">
                <label>Nama Panggilan</label>
                <input value="<?php echo $data['nama_panggilan']; ?>" type="text" name="v_nama_panggilan" class="form-control" />
            </div>
            <div class="form-group">
                <label>Email</label>
                <input value="<?php echo $data['email']; ?>" type="email" name="v_email" class="form-control" />
            </div>
            <div class="form-group">
                <label>Alamat</label>
                <textarea name="v_alamat" class="form-control"><?php echo $data['alamat']; ?></textarea>
            </div>
            <div class="form-group">
                <label>Umur</label>
                <input value="<?php echo $data['umur']; ?>" type="text" name="v_umur" class="form-control" />
            </div>
            <div class="form-group">
                <label>Jabatan</label>
                <input value="<?php echo $data['jabatan']; ?>" type="text" name="v_jabatan" class="form-control" />
            </div>
            <div class="form-group">
                <label>Gaji</label>
                <input value="<?php echo $data['gaji']; ?>" type="text" name="v_gaji" class="form-control" />
            </div>
            <div class="form-group">
                <input type="submit" name="simpan" value="Simpan" />
            </div>
        </form>
    </div>
</html>