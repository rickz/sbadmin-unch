<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('model_universe');

        $session = $_SESSION['user'];
        if (!$session) {
            redirect(base_url('login'));
        }
    }
    
    public function index()
    {
        $data = [];

        // get product
        $data['product'] = $this->model_universe->get_product();

        // application/views/templates/header.php
        $this->load->view('templates/header', $data);
        
        // application/views/v_product.php
        $this->load->view('v_product', $data);

        // application/views/templates/footer.php
        $this->load->view('templates/footer', $data);
    }
    
    public function add()
    {
        $data = [];

        // application/views/templates/header.php
        $this->load->view('templates/header', $data);
        
        // application/views/v_product_add.php
        $this->load->view('v_product_add', $data);

        // application/views/templates/footer.php
        $this->load->view('templates/footer', $data);
    }
    
    public function edit()
    {
        $req = $_REQUEST;
        $data = [];

        // get single product
        $data['product'] = $this->model_universe->get_product_by_id($req['id']);

        // application/views/templates/header.php
        $this->load->view('templates/header', $data);
        
        // application/views/v_product_edit.php
        $this->load->view('v_product_edit', $data);

        // application/views/templates/footer.php
        $this->load->view('templates/footer', $data);
    }

    public function delete()
    {
        $req = $_REQUEST;
        $data = [];

        // delete product
        $data = $this->model_universe->delete_product($req['id']);
        if ($data) {
            echo "
            <script>
                alert('Product has been deleted!');
                location.href = '".base_url()."product';
            </script>
            ";
        
        } else {
            echo "
            <script>
                alert('Product has not been deleted, please try again later.');
                location.href = '".base_url()."product';
            </script>
            ";
        }
    }

    public function action_add()
    {
        $params = $this->input->post();
        $files = $_FILES;
 
        $file_name = $_FILES['thumbnail_product']['name']; // NAMA FILE + FORMAT
        $temporary_dir_file = $_FILES['thumbnail_product']['tmp_name']; // TEMPORARY DIREKTORI
 
        $lokasi_upload_file = "./assets/thumbnail_product/"; // DIREKTORI UNTUK NYIMPAN FILE
 
        $uploaded = move_uploaded_file($temporary_dir_file, $lokasi_upload_file.$file_name); // MEKANIMSE UPLOAD FILE
 
        // VALIDASI KALAU UPLOAD BERHASIL / GAGAL
        if ($uploaded) {
            $params['thumbnail_product'] = base_url().'assets/thumbnail_product/'.$file_name;
        }
        $data = $this->model_universe->create_product($params);
        if ($data) {
            echo "
            <script>
                alert('Product has been added!');
                location.href = '".base_url()."product';
            </script>
            ";
        
        } else {
            echo "
            <script>
                alert('Product has not been added, please try again later.');
                location.href = '".base_url()."product';
            </script>
            ";
        }
    }

    public function action_edit()
    {
        $params = $this->input->post();
        $files = $_FILES;
 
        $file_name = $_FILES['thumbnail_product']['name']; // NAMA FILE + FORMAT
        $temporary_dir_file = $_FILES['thumbnail_product']['tmp_name']; // TEMPORARY DIREKTORI
 
        $lokasi_upload_file = "./assets/thumbnail_product/"; // DIREKTORI UNTUK NYIMPAN FILE
 
        $uploaded = move_uploaded_file($temporary_dir_file, $lokasi_upload_file.$file_name); // MEKANIMSE UPLOAD FILE
 
        // VALIDASI KALAU UPLOAD BERHASIL / GAGAL
        if ($uploaded) {
            $params['thumbnail_product'] = base_url().'assets/thumbnail_product/'.$file_name;
        }
        $data = $this->model_universe->update_product($params['id'], $params);
        if ($data) {
            echo "
            <script>
                alert('Product has been edited!');
                location.href = '".base_url()."product';
            </script>
            ";
        
        } else {
            echo "
            <script>
                alert('Product has not been edited, please try again later.');
                location.href = '".base_url()."product';
            </script>
            ";
        }
    }
}