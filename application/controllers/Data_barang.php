<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_barang extends CI_Controller {
    function __construct(){
        parent::__construct();
        
        $this->load->database();
        $this->load->helper(array('url'));
        $this->load->model('model_crud');
    }

	public function index()
	{
        $data = [];
        $data_barang = $this->model_crud->get_data();
        
        $data['data_barang'] = $data_barang;
        $data['judul'] = 'Data Barang';

        $this->load->view('header', $data);
        $this->load->view('data_barang', $data);
    }
    
    public function tambah()
    {
        $data = [];
        $this->load->view('header', $data);
        $this->load->view('tambah_data_barang', $data);
    }

    public function edit()
    {
        $req = $_REQUEST;
        
        $data = [];
        $data_barang_by_id = $this->model_crud->get_data_by_id($req['id']);
        
        $data['data'] = $data_barang_by_id;
        $this->load->view('header', $data);
        $this->load->view('edit_data_barang', $data);
    }

    public function delete()
    {
        $req = $_REQUEST;
        
        $result = $this->model_crud->delete_data($req['id']);
        if ($result) {
            echo "
            <script>
                alert('Data berhasil dihapus!');
                location.href = '".base_url()."data_barang';
            </script>
            ";

        } else {
            echo "
            <script>
                alert('Mohon maaf data gagal dihapus, silahkan coba lagi nanti.');
                location.href = '".base_url()."data_barang';
            </script>
            ";
        }
    }

    public function action_tambah()
    {
        // $data = $this->input->post();
        $data = $_POST;
        $ins_data = [
            'type' => $data['v_type'],
            'title' => $data['v_title'],
            'banner_image_url' => $data['v_banner_image_url'],
            'description' => $data['v_description'],
            'available_from' => $data['v_available_from'],
            'available_to' => $data['v_available_to'],
            'qrcode_url' => $data['v_qrcode'],
            'product_id' => $data['v_product_id'],
            'category_id' => $data['v_category_id'],
            'position' => $data['v_position'],
        ];
        
        $result = $this->model_crud->insert_data($ins_data);
        if ($result) {
            echo "
            <script>
                alert('Data berhasil ditambahkan!');
                location.href = '".base_url()."data_barang';
            </script>
            ";
        
        } else {
            echo "
            <script>
                alert('Mohon maaf data gagal ditambahkan, silahkan coba lagi nanti.');
                location.href = '".base_url()."data_barang';
            </script>
            ";
        }
    }

    public function action_edit()
    {
        $data = $_POST;
        $ins_data = [
            'type' => $data['v_type'],
            'title' => $data['v_title'],
            'banner_image_url' => $data['v_banner_image_url'],
            'description' => $data['v_description'],
            'available_from' => $data['v_available_from'],
            'available_to' => $data['v_available_to'],
            'qrcode_url' => $data['v_qrcode'],
            'product_id' => $data['v_product_id'],
            'category_id' => $data['v_category_id'],
            'position' => $data['v_position'],
        ];
        
        $result = $this->model_crud->update_data($data['id'], $ins_data);
        if ($result) {
            echo "
            <script>
                alert('Data berhasil disimpan!');
                location.href = '".base_url()."data_barang';
            </script>
            ";

        } else {
            echo "
            <script>
                alert('Mohon maaf data gagal disimpan, silahkan coba lagi nanti.');
                location.href = '".base_url()."data_barang';
            </script>
            ";
        }
    }
}
