<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_karyawan extends CI_Controller {
    function __construct(){
        parent::__construct();
        
        $this->load->database();
        $this->load->helper(array('url'));
        $this->load->model('model_crud');
    }

	public function index()
	{
        $data = [];
        $data_karyawan = $this->model_crud->get_data();
        
        $data['data_karyawan'] = $data_karyawan;
        $data['judul'] = 'Data Karyawan';

        $this->load->view('header', $data);
        $this->load->view('data_karyawan', $data);
    }
    
    public function tambah()
    {
        $data = [];
        $this->load->view('header', $data);
        $this->load->view('tambah_data_karyawan', $data);
    }

    public function edit()
    {
        $req = $_REQUEST;
        
        $data = [];
        $data_karyawan_by_id = $this->model_crud->get_data_by_id($req['id']);
        
        $data['data'] = $data_karyawan_by_id;
        $this->load->view('header', $data);
        $this->load->view('edit_data_karyawan', $data);
    }

    public function delete()
    {
        $req = $_REQUEST;
        
        $result = $this->model_crud->hapus_data($req['id']);
        if ($result) {
            echo "
            <script>
                alert('Data berhasil dihapus!');
                location.href = '".base_url()."data_karyawan';
            </script>
            ";

        } else {
            echo "
            <script>
                alert('Mohon maaf data gagal dihapus, silahkan coba lagi nanti.');
                location.href = '".base_url()."data_karyawan';
            </script>
            ";
        }
    }

    public function action_tambah()
    {
        // $data = $this->input->post();
        $data = $_POST;
        $ins_data = [
            'nama_lengkap' => $data['v_nama_lengkap'],
            'nama_panggilan' => $data['v_nama_panggilan'],
            'umur' => $data['v_umur'],
            'alamat' => $data['v_alamat'],
            'jabatan' => $data['v_jabatan'],
            'gaji' => $data['v_gaji'],
            'email' => $data['v_email'],
        ];
        
        $result = $this->model_crud->insert_data($ins_data);
        if ($result) {
            echo "
            <script>
                alert('Data berhasil ditambahkan!');
                location.href = '".base_url()."data_karyawan';
            </script>
            ";
        
        } else {
            echo "
            <script>
                alert('Mohon maaf data gagal ditambahkan, silahkan coba lagi nanti.');
                location.href = '".base_url()."data_karyawan';
            </script>
            ";
        }
    }

    public function action_edit()
    {
        $data = $_POST;

        // sebelah kiri itu adalah nama kolom pada table mysql
        // sebelah kanan itu nama pada form name="v_nama_lengkap"
        $ins_data = [
            'nama_lengkap' => $data['v_nama_lengkap'],
            'nama_panggilan' => $data['v_nama_panggilan'],
            'umur' => $data['v_umur'],
            'alamat' => $data['v_alamat'],
            'jabatan' => $data['v_jabatan'],
            'gaji' => $data['v_gaji'],
            'email' => $data['v_email'],
        ];
        
        $result = $this->model_crud->update_data($data['v_id'], $ins_data);
        if ($result) {
            echo "
            <script>
                alert('Data berhasil disimpan!');
                location.href = '".base_url()."data_karyawan';
            </script>
            ";

        } else {
            echo "
            <script>
                alert('Mohon maaf data gagal disimpan, silahkan coba lagi nanti.');
                location.href = '".base_url()."data_karyawan';
            </script>
            ";
        }
    }
}
