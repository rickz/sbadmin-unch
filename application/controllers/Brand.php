<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Brand extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('model_universe');

        $session = $_SESSION['user'];
        if (!$session) {
            redirect(base_url('login'));
        }
    }
    
    public function index()
    {
        $data = [];

        // get product
        $data['brand'] = $this->model_universe->get_brand();

        // application/views/templates/header.php
        $this->load->view('templates/header', $data);
        
        // application/views/v_product.php
        $this->load->view('v_brand', $data);

        // application/views/templates/footer.php
        $this->load->view('templates/footer', $data);
    }
    
    public function add()
    {
        $data = [];

        // application/views/templates/header.php
        $this->load->view('templates/header', $data);
        
        // application/views/v_product_add.php
        $this->load->view('v_brand_add', $data);

        // application/views/templates/footer.php
        $this->load->view('templates/footer', $data);
    }
    
    public function edit()
    {
        $req = $_REQUEST;
        $data = [];

        // get single product
        $data['brand'] = $this->model_universe->get_brand_by_id($req['id']);

        // application/views/templates/header.php
        $this->load->view('templates/header', $data);
        
        // application/views/v_product_edit.php
        $this->load->view('v_brand_edit', $data);

        // application/views/templates/footer.php
        $this->load->view('templates/footer', $data);
    }

    public function delete()
    {
        $req = $_REQUEST;
        $data = [];

        // delete product
        $data = $this->model_universe->delete_brand($req['id']);
        if ($data) {
            echo "
            <script>
                alert('The brand has been deleted!');
                location.href = '".base_url()."brand';
            </script>
            ";
        
        } else {
            echo "
            <script>
                alert('The brand has not been deleted, please try again later.');
                location.href = '".base_url()."brand';
            </script>
            ";
        }
    }

    public function action_add()
    {
        $params = $this->input->post();
        $files = $_FILES;
 
        $file_name = $_FILES['brand_image']['name']; // NAMA FILE + FORMAT
        $temporary_dir_file = $_FILES['brand_image']['tmp_name']; // TEMPORARY DIREKTORI
 
        $lokasi_upload_file = "./assets/brand_image/"; // DIREKTORI UNTUK NYIMPAN FILE
 
        $uploaded = move_uploaded_file($temporary_dir_file, $lokasi_upload_file.$file_name); // MEKANIMSE UPLOAD FILE
 
        // VALIDASI KALAU UPLOAD BERHASIL / GAGAL
        if ($uploaded) {
            $params['brand_image'] = base_url().'assets/brand_image/'.$file_name;
        }
        $data = $this->model_universe->create_brand($params);
        if ($data) {
            echo "
            <script>
                alert('The brand has been added!');
                location.href = '".base_url()."brand';
            </script>
            ";
        
        } else {
            echo "
            <script>
                alert('The brand has not been added, please try again later.');
                location.href = '".base_url()."brand';
            </script>
            ";
        }
    }

    public function action_edit()
    {
        $params = $this->input->post();
        $files = $_FILES;
 
        $file_name = $_FILES['brand_image']['name']; // NAMA FILE + FORMAT
        $temporary_dir_file = $_FILES['brand_image']['tmp_name']; // TEMPORARY DIREKTORI
 
        $lokasi_upload_file = "./assets/brand_image/"; // DIREKTORI UNTUK NYIMPAN FILE
 
        $uploaded = move_uploaded_file($temporary_dir_file, $lokasi_upload_file.$file_name); // MEKANIMSE UPLOAD FILE
 
        // VALIDASI KALAU UPLOAD BERHASIL / GAGAL
        if ($uploaded) {
            $params['brand_image'] = base_url().'assets/brand_image/'.$file_name;
        }
        $data = $this->model_universe->update_brand($params['id'], $params);
        if ($data) {
            echo "
            <script>
                alert('The brand has been edited!');
                location.href = '".base_url()."brand';
            </script>
            ";
        
        } else {
            echo "
            <script>
                alert('The brand has not been edited, please try again later.');
                location.href = '".base_url()."brand';
            </script>
            ";
        }
    }
}