<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('model_global');

        $session = $_SESSION['user'];
        if (!$session) {
            redirect(base_url('login'));
        }
    }
    
    public function index()
	{
        $data = [];

        // application/views/templates/header.php
		$this->load->view('templates/header', $data);
        
        // application/views/v_dashboard.php
        $this->load->view('v_dashboard');

        // application/views/templates/footer.php
		$this->load->view('templates/footer');
    }
    
    public function save_profile()
    {
        $params = $this->input->post();
        $data = $this->model_global->update_profile($params);
        if ($data) {
            echo "
            <script>
                alert('Profile has been saved!');
                location.href = '".base_url()."dashboard';
            </script>
            ";
        
        } else {
            echo "
            <script>
                alert('Profile has not been saved, please try again later.');
                location.href = '".base_url()."dashboard';
            </script>
            ";
        }
    }
}