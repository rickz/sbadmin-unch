<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('model_global');
    }
    
    public function index()
    {
        $data = [];

        $this->load->view('v_register');
    }
    
    public function validate()
    {
        $params = $this->input->post();
        $data = $this->model_global->register($params);
            echo "
            <script>
                alert('Your account has been successfully created, please login.')
                location.href = '".base_url()."login';
            </script>
            ";
        
        } else {
            echo "
            <script>
                alert('Your account didn't successfully created, please try again.');
                location.href = '".base_url()."register';
            </script>
            ";
        }
    }
}