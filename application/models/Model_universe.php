<?php
class model_universe extends CI_Model {
    public function __construct()
    {
        $this->load->database();
    }

    public function get_product()
    {
    	$result = $this->db->get('product')->result_array();

    	return $result;
    }

    public function get_category()
    {
    	$result = $this->db->get('category')->result_array();

    	return $result;
    }

    public function get_brand()
    {
    	$result = $this->db->get('brand')->result_array();

    	return $result;
    }

    public function get_product_by_id($id)
    {
		$this->db->where('id', $id);
        $result = $this->db->get('product')->row_array();
		
		return $result;
	}

	public function get_category_by_id($id)
    {
		$this->db->where('id', $id);
        $result = $this->db->get('category')->row_array();
		
		return $result;
	}

	public function get_brand_by_id($id)
    {
		$this->db->where('id', $id);
        $result = $this->db->get('brand')->row_array();
		
		return $result;
	}

	public function create_product($data)
    {
		$result = $this->db->insert('product', $data);
		
		return $result;
	}

		public function create_category($data)
    {
		$result = $this->db->insert('category', $data);
		
		return $result;
	}

			public function create_brand($data)
    {
		$result = $this->db->insert('brand', $data);
		
		return $result;
	}

	public function update_product($id, $data)
    {
		$this->db->where('id', $id);
		$result = $this->db->update('product', $data);
		
		return $result;
	}

	public function update_category($id, $data)
    {
		$this->db->where('id', $id);
		$result = $this->db->update('category', $data);
		
		return $result;
	}

	public function update_brand($id, $data)
    {
		$this->db->where('id', $id);
		$result = $this->db->update('brand', $data);
		
		return $result;
	}

	public function delete_product($id)
	{
		$this->db->where('id', $id);
		$result = $this->db->delete('product');
		
		return $result;
	}

	public function delete_category($id)
	{
		$this->db->where('id', $id);
		$result = $this->db->delete('category');
		
		return $result;
	}

	public function delete_brand($id)
	{
		$this->db->where('id', $id);
		$result = $this->db->delete('brand');
		
		return $result;
	}
}